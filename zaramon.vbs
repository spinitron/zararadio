' Copyright (c) 2011 Spinitron All Rights Reserved
'
' THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
' IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
' ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
' LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
' CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
' SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
' INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
' CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
' ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
' THE POSSIBILITY OF SUCH DAMAGE.
'
' Contact eva@spinitron.com for assistance.
'
' This runs from the Windows command prompt with CScript as follows:
' C:\Path\to\dir>cscript zaramon.vbs
' assuming C:\Path\to\dir is the directory with zaramon.vbs in it.
'
' You must configure the following items:
'
' Drive letter of where the Zara logs are located
drv = "C:"
'
' Log file directory in drv. Use double backslash as dir separator
dir = "\\Program Files\\ZaraSoft\\ZaraRadio\\Logs\\"
'
' Your station ID in Spinitron, usually your call sign in lower case
stn = "klmn"
'
' Email address of the user account Zara wil use to log playlists
usr = "user@klmn.org"
'
' Password of that user account
pas = "guessme"
'
' End of configurations. Leave the rest of the file alone.
'

WScript.Echo "Monitoring " & drv & dir

spinurl = "http://spinitron.com/member/logthis.php?" & _
  "st=" & Escape(stn) & "&" & "un=" & Escape(usr) & "&" & "pw=" & Escape(pas)

Set oFSO  = CreateObject("Scripting.FileSystemObject") 
Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
Set oHTTP = CreateObject("MSXML2.XMLHTTP")
Set colMonitoredEvents = objWMIService.ExecNotificationQuery( _
  "SELECT * FROM __InstanceModificationEvent WITHIN 5 WHERE " _
  & "TargetInstance ISA 'CIM_DataFile' AND TargetInstance.Path='" & dir & "'" _
)

lastline = "unlikely_value"
Do
  Set i = colMonitoredEvents.NextEvent.TargetInstance
  line = ""
  If oFSO.FileExists(i.Name) Then 
    Set oFile = oFSO.OpenTextFile(i.Name, 1) 
    Do While Not oFile.AtEndOfStream 
      sText = oFile.ReadLine 
      If Trim(sText) <> "" Then 
        line = sText 
      End If 
    Loop 
    oFile.Close 
  Else
    WScript.Echo "Failed to read " & i.Name
  End If 
  If line <> "" And lastline <> line Then
    lastline = line
    r = spinurl & "&zara=" & Escape(line)
    WScript.Echo "Sending: '" & r & "'"
    oHTTP.Open "GET", r, False
    On Error Resume Next
    oHTTP.Send
    WScript.Echo "Got: " & oHTTP.status & " " & oHTTP.statusText
  End If
Loop
