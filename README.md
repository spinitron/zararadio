# Zaramon

This visual basic script monitors ZaraRadio Free's log files to find song metadata when the now playing song changes and send that metadata to Spinitron.

## Installation and configuration

Save `zaramon.vbs` in a directory on the computer running ZaraRadio Free, e.g.
  `C:\Program Files\Spinitron\zaramon.vbs`
Open it up using Notepad or some other text editor.
Update the following 5 variables to values appropriate for your situation:

* `drv` is drive letter of the Zara logs directory, e.g.
  `drv = "C:"`
* `dir` is the path to the Zara logs directory. Use double backslash as separator, e.g.
  `dir = "\\Program Files\\ZaraSoft\\ZaraRadio\\Logs\\"`  
* `stn` is your station ID in Spinitron, usually your call sign in lower case, e.g.
  `stn = "klmn"`
* `usr` is the email address of the user account Zara will use to log playlists, e.g.
  `usr = "user@somewhere.com"`
* `pas` is the password of that user account, e.g.
  `pas = "guessme"`

## Operation

Start the monitor script as follows:

* In the Windows Start menu, choose "Run…".
* Type `cmd` and press Enter to open a command window.
* In the command window, type `cscript` followed by a space followed by the full path to the `zaramon.vbs` script file, e.g.  

        C:\>cscript C:\Program Files\Spinitron\zaramon.vbs

Stop the monitor script by pressing `Ctrl-C` in the command window in which `zaramon.vbs` is running. Closing the script's command window  will stop the script so it's important that users don't do that inadvertently.

When it is started the script prints a message saying what directory it is monitoring for Zara log files.

It also prints two messages every time it sends information to Spinitron. The first is the message it sends and the second is response it got from Spinitron. If the response is `Got: 200 OK` then Spinitron accepted the message. If it is anything else then there is a problem.

Some communications errors will cause the script to terminate with a system error message. This most likely means that the script was not able to reach Spinitron. The cause might be that the script is not properly configured or that there is a problem with network connectivity.
    
If you are unable to resolve problems with the script, contact Spinitron for support.